PROJECT ?= $(notdir $(CURDIR))
PROJECT := $(strip $(PROJECT))
PROJECT := $(subst -,_,$(PROJECT))

requirements.txt:
	@echo "django" > requirements.txt
	@echo "django-compressor" >> requirements.txt

env/bin/activate:
	virtualenv env || virtualenv2 env

env/bin/django-admin: | env/bin/activate requirements.txt
	./env/bin/pip install -r requirements.txt

$(PROJECT)/settings.py: | env/bin/activate env/bin/django-admin
	@./env/bin/django-admin startproject $(PROJECT) .

export SETTINGS
export DEFAULT_SETTINGS
$(PROJECT)/default_settings.py: | $(PROJECT)/settings.py
	@echo "$$SETTINGS" > $(PROJECT)/default_settings.py
	@echo "$$DEFAULT_SETTINGS" >> $(PROJECT)/settings.py

export PACKAGE_JSON
package.json:
	@echo "$$PACKAGE_JSON" > package.json 

export GIT_IGNORE
.gitignore:
	@echo "$$GIT_IGNORE" > .gitignore

node_modules: | package.json
	npm install

export MAIN_SCSS
assets/stylesheets:
	@mkdir -p assets/stylesheets/base
	@mkdir -p assets/stylesheets/components
	@mkdir -p assets/stylesheets/pages
	@echo "$$MAIN_SCSS" > assets/stylesheets/main.scss

assets/src:
	@mkdir -p assets/src/components
	@touch assets/src/app.js

.PHONY = setup
setup: | requirements.txt $(PROJECT)/default_settings.py node_modules \
	.gitignore assets/stylesheets assets/src

.PHONY = run
run: 
	env/bin/python manage.py runserver

.PHONY = migrate
migrate: 
	env/bin/python manage.py migrate

# Templates

define MAIN_SCSS
@import "base/*"
@import "components/*"
@import "pages/*"
endef

define GIT_IGNORE
/static
/db.sqlite3
/env
/node_modules
/media
/static

*.py[cod]
.DS_Store
.module-cache
.sass-cache
local_settings.py
endef


define SETTINGS
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'compressor.finders.CompressorFinder',
)

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "assets"),
)

STATIC_ROOT = os.path.join(BASE_DIR, 'static')

COMPRESS_OFFLINE = True

COMPRESS_OUTPUT_DIR = ''

COMPRESS_PRECOMPILERS = (
    ('text/x-scss', 'sassc {infile} {outfile}'),
    ('text/jsx', 'browserify --debug -t reactify {infile} -o {outfile}'),
)

try:
	from local_settings import *
except ImportError:
	pass
endef


define DEFAULT_SETTINGS

# Injected by django.mk

from default_settings import *
endef

define PACKAGE_JSON
{
    "name" : "$(PROJECT)",
    "dependencies": {
        "react": "*",
        "reactify": "*"
    }
}
endef
