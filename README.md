# A collection of dandy makefiles

## Django

### Requirements

`virtualenv`, `npm`, `sassc` and `browserify`

### Instructions

1. Create a new directory for your project. Move inside and issue the
   following command to download the Django-specific Makefile.

        wget https://bitbucket.org/trelltech/trell-makefiles/raw/master/django.mk

2. Create a `Makefile` containing the following:
    
        PROJECT = my_project
        
        include django.mk

3. And run `setup`:

        make setup

4. Add `compressor` to `INSTALLED_APPS` in the generated `settings.py`.
